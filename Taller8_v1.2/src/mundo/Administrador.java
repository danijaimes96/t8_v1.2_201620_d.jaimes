package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import dnarvaez27.list.linkedlist.DoubleLinkedList;
import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador
{

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS = "./data/stations.txt";

	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS = "./data/routes.txt";

	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	// TODO Declare el atributo grafo No dirigido que va a modelar el sistema.
	// Los identificadores de las estaciones son String
	public GrafoNoDirigido<String, String> grafo;

	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador()
	{
		// TODO inicialice el grafo como un GrafoNoDirigido;
		grafo = new GrafoNoDirigido<>();
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * 
	 * @param identificador
	 * @return Arreglo con los identificadores de las rutas que pasan por la
	 *         estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		// TODO Implementar

		DoubleLinkedList<Arco<String, String>> aayyy = new DoubleLinkedList<>();
		for (Arco<String, String> a : grafo.darArcosOrigen(identificador))
		{
			aayyy.add(a);
		}
		for (Arco<String, String> b : grafo.darArcosDestino(identificador))
		{
			aayyy.add(b);
		}
		int i = 0;
		String[] retornar = new String[aayyy.size()];
		for (Arco<String, String> arco : aayyy)
		{
			retornar[i++] = arco.darInformacion();
		}
		return retornar;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del
	 * sistema.
	 * 
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		// TODO Implementar
		double min = Double.MAX_VALUE;
		for (Arco<String, String> a : grafo.darArcos())
		{
			if (a.darCosto() < min)
			{
				min = a.darCosto();
			}
		}
		return min;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del
	 * sistema.
	 * 
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()

	{
		// TODO Implementar
		double max = 0;
		for (Arco<String, String> a : grafo.darArcos())
		{
			if (a.darCosto() > max)
			{
				max = a.darCosto();
			}
		}
		return max;
	}

	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el
	 * grafo
	 * 
	 * @throws Exception
	 *             si ocurren problemas con la lectura de los archivos o si los
	 *             archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		// TODO Implementar
		BufferedReader leer = new BufferedReader(
				new FileReader(RUTA_PARADEROS));
		int tam = Integer.parseInt(leer.readLine());
		for (int i = 0; i < tam; i++)
		{
			String[] estacioncita = leer.readLine().split(";");
			grafo.agregarNodo(new Estacion<String>(estacioncita[0],
					Double.parseDouble(estacioncita[1]),
					Double.parseDouble(estacioncita[2])));
		}
		System.out.println("Se han cargado correctamente "
				+ grafo.darNodos().length + " Estaciones");

		// TODO Implementar
		BufferedReader leer2 = new BufferedReader(new FileReader(RUTA_RUTAS));
		int tam2 = Integer.parseInt(leer2.readLine());

		for (int j = 0; j < tam2; j++)
		{
			leer2.readLine();
			String nombre = leer2.readLine();
			int tam3 = Integer.parseInt(leer2.readLine());
			String whate = "";
			for (int k = 0; k < tam3; k++)
			{
				String[] estacioncita = leer2.readLine().split(" ");
				if (!whate.isEmpty())
				{
					grafo.agregarArco(whate, estacioncita[0],
							Double.parseDouble(estacioncita[1]), nombre);
				}
			
					whate=estacioncita[0];
				
			}
		}

		System.out.println("Se han cargado correctamente "
				+ grafo.darArcos().length + "arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * 
	 * @param identificador
	 *            de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo
	 *         contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		// TODO Implementar
		return (Estacion<String>) grafo.buscarNodo(identificador);

	}

}
