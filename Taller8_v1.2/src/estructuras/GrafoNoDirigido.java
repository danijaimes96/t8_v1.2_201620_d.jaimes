package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sun.org.apache.xerces.internal.impl.xs.util.LSInputListImpl;

import dnarvaez27.elements.Entry;
import dnarvaez27.hashtable.HashTableSC;
import dnarvaez27.list.linkedlist.DoubleLinkedList;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * 
 * @param K
 *            tipo del identificador de los vertices (Comparable)
 * @param E
 *            tipo de la informacion asociada a los arcos
 * 
 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K, E>
{

	/**
	 * Nodos del grafo
	 */
	// TODO Declare la estructura que va a contener los nodos
	private HashTableSC<K, Nodo<K>> noditos;

	/**
	 * Lista de adyacencia
	 */
	private HashMap<K, DoubleLinkedList<Arco<K, E>>> lAdj;

	// TODO Utilice sus propias estructuras (defina una representacion para el
	// grafo)
	// Es libre de implementarlo con la representacion de su agrado.

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido()
	{
		// TODO implementar
		noditos = new HashTableSC<>();
		lAdj = new HashMap<>();

	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo)
	{
		// TODO implementar
		if (noditos.get(nodo.darId()) == null)
		{
			noditos.put(nodo.darId(), nodo);
			return true;
		}
		return false;
	}

	@Override
	public boolean eliminarNodo(K id)
	{
		// TODO implementar
		if (noditos.get(id) != null)
		{
			noditos.delete(id);
			lAdj.remove(id);
			for (DoubleLinkedList<Arco<K, E>> a : lAdj.values())
			{
				for (Arco<K, E> arco : a)
				{
					if (arco.darNodoFin().darId().equals(id))
					{
						a.remove(arco);
						break;
					}
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public Arco<K, E>[] darArcos()
	{
		// TODO implementar
		DoubleLinkedList<Arco> arquitos = new DoubleLinkedList<Arco>();
		for (DoubleLinkedList<Arco<K, E>> a : lAdj.values())
		{
			for (Arco<K, E> arco : a)
			{
				arquitos.add(arco);
			}
		}
		int tam = arquitos.size();
		int pos = 0;
		Arco<K, E>[] arreglito = new Arco[tam];
		for (Arco<K, E> arco : arquitos)
		{
			arreglito[pos++] = arco;
		}
		return arreglito;

	}

	private Arco<K, E> crearArco(K inicio, K fin, double costo, E e)
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if (nodoI != null && nodoF != null)
		{
			return new Arco<K, E>(nodoI, nodoF, costo, e);
		}
		return null;
	}

	@Override
	public Nodo<K>[] darNodos()
	{
		Nodo<K>[] dnoditos = new Nodo[noditos.size()];
		int i = 0;
		for (Entry<K, Nodo<K>> e : noditos.entries())
		{
			dnoditos[i++] = (e.getValue());
		}
		return dnoditos;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj)
	{
		// TODO implementar
		// todo aqui queeedeee!zzz
		DoubleLinkedList<Arco<K, E>> arc = lAdj.get(inicio);
		if (arc == null)
		{
			arc = new DoubleLinkedList<>();
		}
		Arco<K, E> a = crearArco(inicio, fin, costo, obj);
		if (a == null)
		{
			return false;
		}
		arc.add(a);
		lAdj.put(inicio, arc);
		return true;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo)
	{
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K, E> eliminarArco(K inicio, K fin)
	{
		// TODO implementar
		DoubleLinkedList<Arco<K, E>> listica = lAdj.get(inicio);
		for (Arco<K, E> a : listica)
		{
			if (a.darNodoFin().darId().equals(fin))
			{
				lAdj.get(inicio).remove(a);
				return a;
			}
		}
		return null;

	}

	@Override
	public Nodo<K> buscarNodo(K id)
	{
		// TODO implementar
		return noditos.get(id);

	}

	@Override
	public Arco<K, E>[] darArcosOrigen(K id)
	{
		// TODO implementar
		DoubleLinkedList<Arco<K, E>> list = lAdj.get(id);
		Arco<K, E>[] arquitooos = new Arco[list.size()];
		int i = 0;
		for (Arco<K, E> arco : list)
		{
			arquitooos[i++] = arco;
		}
		return arquitooos;
	}

	@Override
	public Arco<K, E>[] darArcosDestino(K id)
	{
		// TODO implementar

		DoubleLinkedList<Arco<K, E>> listafea = new DoubleLinkedList<>();
		for (DoubleLinkedList<Arco<K, E>> listita : lAdj.values())
		{
			for (Arco<K, E> arco : listita)
			{
				if (arco.darNodoFin().darId().equals(id))
				{
					listafea.add(arco);
				}
			}
		}
		Arco<K, E>[] arquitooos = new Arco[listafea.size()];
		int i = 0;
		for (Arco<K, E> arco : listafea)
		{
			arquitooos[i++] = arco;
		}
		return arquitooos;
	}

}
